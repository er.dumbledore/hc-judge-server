#!/bin/bash
rm -rf /judger/run/*
rm -rf /judger/spj/*

# mkdir -p /judger/run /judger/spj

# chown compiler:code /judger/run
# chmod 711 /judger/run

# chown compiler:spj /judger/spj
# chmod 710 /judger/spj

core=$(grep --count ^processor /proc/cpuinfo)
n=$(($core*2))

export LC_ALL=C.UTF-8
export LANG=C.UTF-8
export FLASK_APP=server.py
export FLASK_DEBUG=1
# exec flask run --host=0.0.0.0 --port=8080
exec gunicorn --capture-output --log-file '-' --enable-stdio-inheritance --log-level "debug" --workers 2 --time 600 --bind 0.0.0.0:8080 server:app
