#!/bin/bash

echo "############"
echo "Make sure that you have activated the virtual env"
echo "############"

export LC_ALL=en_US.utf8
export LANG=en_US.utf8

export FLASK_APP=server.py
export FLASK_DEBUG=1
export judger_debug=1

export BACKEND_URL=http://backend:80/api/judge_server_heartbeat
export SERVICE_URL=http://judge-server:12358
export TOKEN=abc123

# Use the compiled file name: "__pycache__/solution.cpython-36.pyc"

flask run -h 0.0.0.0 -p 12358