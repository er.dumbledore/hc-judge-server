# JudgeServer

## Dev setup (without Docker)

Install Linux dependencies

```sh
sudo yum install libseccomp-devel

cd /tmp 
git clone -b newnew  --depth 1 https://github.com/QingdaoU/Judger 
cd Judger 
 
mkdir build 
cd build 
cmake .. 
make 
make install 
cd ../bindings/Python 
python3 setup.py install 
 
useradd -u 12001 compiler 
useradd -u 12002 code 
useradd -u 12003 spj 
usermod -a -G code sp
```

Make required directories and give permissions

```sh
rm -rf /judger/*
mkdir -p /judger/run /judger/spj

chown compiler:code /judger/run
chmod 711 /judger/run

chown compiler:spj /judger/spj
chmod 710 /judger/spj

sudo mkdir /code
sudo chown ec2-user:ec2-user /code

sudo mkdir /test_case
sudo chown ec2-user:ec2-user /test_case
```

Make virtual env

```sh
python3 --version  # same will be used for compiling python programs
python3 -m venv ~/Envs/hc-judge-server

source ~/Envs/hc-judge-server/bin/activate

pip install --no-cache-dir psutil gunicorn flask requests
```