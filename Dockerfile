FROM ubuntu:16.04

COPY build/java_policy /etc

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:jonathonf/python-3.6 && apt-get update

RUN buildDeps='software-properties-common git libtool cmake python3.6-dev python3-pip python-pip libseccomp-dev' && \
    apt-get install -y python python3.6 python3.6-venv python-pkg-resources python3-pkg-resources gcc g++ $buildDeps

RUN add-apt-repository ppa:openjdk-r/ppa && apt-get update && apt-get install -y openjdk-8-jdk

#RUN python3.6 -m venv $HOME/Envs/py36 && . $HOME/Envs/py36/bin/activate && \
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.5 1 && \
    update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 2 && \
    update-alternatives --set python3 /usr/bin/python3.6 && \
    python3 -m pip install --upgrade pip && \
    python3 -m pip install --no-cache-dir psutil gunicorn flask requests numpy scipy && \
    cd /tmp && \
    git clone -b newnew --depth 1 https://github.com/QingdaoU/Judger && cd Judger && \
    mkdir build && cd build && cmake .. && make && make install && cd ../bindings/Python && \
    python3 setup.py install

RUN buildDeps='software-properties-common git libtool cmake python3.6-dev python3-pip python-pip libseccomp-dev' && \
    apt-get purge -y --auto-remove $buildDeps && \
    apt-get clean && rm -rf /var/lib/apt/lists/* && \
    mkdir -p /code

RUN useradd -u 12001 compiler && useradd -u 12002 code && useradd -u 12003 spj && usermod -a -G code spj

HEALTHCHECK --interval=5s --retries=3 CMD python3 /code/service.py
ADD server /code
WORKDIR /code
RUN gcc -shared -fPIC -o unbuffer.so unbuffer.c
EXPOSE 8080
ENTRYPOINT /code/entrypoint.sh
